#include <iostream>
#include <time.h>
#include <conio.h>
#include <cmath>
#include <string>
#include <Windows.h>
#include <omp.h>
#define M_PI 3.14159265358979323846

using namespace std;

double func(double z,double x, double y)
{
	double wynik1, wynik2;
	
	wynik1 = (sin(x) * sin(y))/(sqrt(z*z+x*x+y*y));
	wynik2 = abs(wynik1);
	return wynik2;
}


int main()
{
	float xp, xk, dx, yp, yk, dy, calka,wynik;
	int n;
	float clock1,clock2,clock3;

	xp = 0.0;
	xk = 20 * M_PI;
	n = 1000;
	calka = 0.0;
	wynik = 0.0;

	dx = (xk - xp) / (float)n;
	dy = (xk- xp) / (float)n;
	clock1 = clock();

	for (int z = 1; z <= 100; z++)   //z
	{	
		wynik = 0;
#pragma omp parallel for reduction(+:wynik)
		for (int j = 1; j <= n; j++)    //x
		{	
			calka = 0;
#pragma omp parallel for reduction(+:calka) 
			for (int i = 1; i <= n; i++)    //y
			{	
				calka = calka + func(z, xp + j*dx, xp + i*dy)*dx*dy;
			}
			wynik += calka;
		}
#pragma omp critical	
		printf("Wynik: %f \n",wynik);
	}

	clock2 = clock();
	clock3 = (clock2 - clock1)/1000;
	cout << "Czas trwania programu: " << clock3<<"s";

	system("pause");
}